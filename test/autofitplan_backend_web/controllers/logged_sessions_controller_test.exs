defmodule AutofitplanBackendWeb.LoggedSessionsControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/logged-sessions", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/logged-sessions")
    get(conn, "/api/logged-sessions")
  end

  test "GET /api/logged-sessions/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/logged-sessions/3")
    get(conn, "/api/logged-sessions/3")
  end

  test "POST /api/logged-sessions", %{conn: conn} do
    ProxyHelpers.assert_proxy("POST", "/api/logged-sessions")
    post(conn, "/api/logged-sessions")
  end

  test "PATCH /api/logged-sessions/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("PATCH", "/api/logged-sessions/3")
    patch(conn, "/api/logged-sessions/3")
  end

  test "OPTIONS /api/logged-sessions", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/logged-sessions")
    options(conn, "/api/sessions")
  end
end
