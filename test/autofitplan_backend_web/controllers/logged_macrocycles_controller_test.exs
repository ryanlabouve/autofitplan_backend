defmodule AutofitplanBackendWeb.LoggedMacrocyclesControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/logged-macrocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/logged-macrocycles")
    get(conn, "/api/logged-macrocycles")
  end

  test "GET /api/logged-macrocycles/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/logged-macrocycles/3")
    get(conn, "/api/logged-macrocycles/3")
  end

  test "POST /api/logged-macrocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("POST", "/api/logged-macrocycles")
    post(conn, "/api/logged-macrocycles")
  end

  test "PATCH /api/logged-macrocycles/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("PATCH", "/api/logged-macrocycles/3")
    patch(conn, "/api/logged-macrocycles/3")
  end

  test "DELETE /api/logged-macrocycles/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("DELETE", "/api/logged-macrocycles/3")
    delete(conn, "/api/logged-macrocycles/3")
  end

  test "OPTIONS /api/logged-macrocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/logged-macrocycles")
    options(conn, "/api/macrocycles")
  end
end
