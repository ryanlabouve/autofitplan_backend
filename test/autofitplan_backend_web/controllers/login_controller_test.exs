defmodule AutofitplanBackendWeb.LoginControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "POST /login", %{conn: conn} do
    ProxyHelpers.assert_proxy("POST", "/api/login")

    post(conn, "/api/login")
  end
end
