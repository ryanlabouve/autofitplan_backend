defmodule AutofitplanBackendWeb.MacrocyclesControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/macrocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/macrocycles")
    get(conn, "/api/macrocycles")
  end

  test "GET /api/macrocycles/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/macrocycles/3")
    get(conn, "/api/macrocycles/3")
  end

  test "OPTIONS /api/macrocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/macrocycles")
    options(conn, "/api/macrocycles")
  end
end
