defmodule AutofitplanBackendWeb.SessionsControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/sessions", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/sessions")
    get(conn, "/api/sessions")
  end

  test "GET /api/sessions/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/sessions/3")
    get(conn, "/api/sessions/3")
  end

  test "OPTIONS /api/sessions", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/sessions")
    options(conn, "/api/sessions")
  end
end
