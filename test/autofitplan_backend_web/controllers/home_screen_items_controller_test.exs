defmodule AutofitplanBackendWeb.HomeScreenItemsControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/home-screen-items", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/home-screen-items")
    get(conn, "/api/home-screen-items")
  end
end
