defmodule AutofitplanBackendWeb.UsersControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/users", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/users")
    get(conn, "/api/users")
  end

  test "GET /api/users/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/users/3")
    get(conn, "/api/users/3")
  end

  test "PATCH /api/users/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("PATCH", "/api/users/3")
    patch(conn, "/api/users/3")
  end

  test "GET /api/users/me", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/users/me")
    get(conn, "/api/users/me")
  end

  test "OPTIONS /api/users/me", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/users/me")
    options(conn, "/api/users/me")
  end
end
