defmodule AutofitplanBackendWeb.TokenControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "POST /api/token", %{conn: conn} do
    ProxyHelpers.assert_proxy("POST", "/api/token")
    post(conn, "/api/token")
  end

  test "OPTIONS /api/token", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/token")
    options(conn, "/api/token")
  end
end
