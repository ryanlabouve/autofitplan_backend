defmodule AutofitplanBackendWeb.MesocyclesControllerTest do
  use AutofitplanBackendWeb.ConnCase

  require ProxyHelpers

  test "GET /api/mesocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/mesocycles")
    get(conn, "/api/mesocycles")
  end

  test "GET /api/mesocycles/:id", %{conn: conn} do
    ProxyHelpers.assert_proxy("GET", "/api/mesocycles/3")
    get(conn, "/api/mesocycles/3")
  end

  test "OPTIONS /api/mesocycles", %{conn: conn} do
    ProxyHelpers.assert_proxy("OPTIONS", "/api/mesocycles")
    options(conn, "/api/mesocycles")
  end
end
