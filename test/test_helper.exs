Application.ensure_all_started(:mox)
Mox.defmock(AutofitplanBackend.LegacyProxyMock, for: AutofitplanBackend.LegacyProxy)

ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(AutofitplanBackend.Repo, :manual)

defmodule ProxyHelpers do
  defmacro assert_proxy(method, endpoint) do
    quote do
      import Mox

      AutofitplanBackend.LegacyProxyMock
      |> expect(:proxy, fn conn, opts ->
        verb_correct = Map.fetch!(opts, :method) === unquote(method)
        endpoint_correct = Map.fetch!(opts, :endpoint) === unquote(endpoint)

        assert verb_correct
        assert endpoint_correct

        conn
      end)
    end
  end
end
