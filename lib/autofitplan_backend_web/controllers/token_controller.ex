defmodule AutofitplanBackendWeb.TokenController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def create(conn, _opts) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/token", method: "POST"})
  end
end
