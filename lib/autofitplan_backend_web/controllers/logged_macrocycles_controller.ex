defmodule AutofitplanBackendWeb.LoggedMacrocyclesController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def index(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-macrocycles", method: "GET"})
  end

  def create(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-macrocycles", method: "POST"})
  end

  @spec show(atom | %{path_params: map}, any) :: any
  def show(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-macrocycles/#{id}", method: "GET"})
  end

  def update(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-macrocycles/#{id}", method: "PATCH"})
  end

  def delete(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-macrocycles/#{id}", method: "DELETE"})
  end

  def options(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-macrocycles", method: "OPTIONS"})
  end
end
