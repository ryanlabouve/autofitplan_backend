defmodule AutofitplanBackendWeb.HomeScreenItemsController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def index(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/home-screen-items", method: "GET"})
  end
end
