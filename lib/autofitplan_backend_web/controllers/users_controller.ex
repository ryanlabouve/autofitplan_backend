defmodule AutofitplanBackendWeb.UsersController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def index(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/users", method: "GET"})
  end

  def show(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/users/#{id}", method: "GET"})
  end

  def options(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/users", method: "OPTIONS"})
  end

  def update(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/users/#{id}", method: "PATCH"})
  end

  def me(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/users/me", method: "GET"})
  end
end
