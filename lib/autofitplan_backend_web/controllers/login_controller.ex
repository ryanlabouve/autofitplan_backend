defmodule AutofitplanBackendWeb.LoginController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  @spec create(Plug.Conn.t(), any) :: Plug.Conn.t()
  def create(conn, _params) do
    conn
    |> LegacyProxy.proxy(%{endpoint: "/api/login", method: "POST"})
  end
end
