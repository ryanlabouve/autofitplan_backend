defmodule AutofitplanBackendWeb.PingController do
  use AutofitplanBackendWeb, :controller

  @spec index(Plug.Conn.t(), any) :: Plug.Conn.t()
  def index(_conn, _params) do
  end
end
