defmodule AutofitplanBackendWeb.SessionsController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def index(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/sessions", method: "GET"})
  end

  def show(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/sessions/#{id}", method: "GET"})
  end

  def options(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/sessions", method: "OPTIONS"})
  end
end
