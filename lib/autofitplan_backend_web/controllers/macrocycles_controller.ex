defmodule AutofitplanBackendWeb.MacrocyclesController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def index(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/macrocycles", method: "GET"})
  end

  def show(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/macrocycles/#{id}", method: "GET"})
  end

  def options(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/macrocycles", method: "OPTIONS"})
  end
end
