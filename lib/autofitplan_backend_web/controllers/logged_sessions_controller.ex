defmodule AutofitplanBackendWeb.LoggedSessionsController do
  use AutofitplanBackendWeb, :controller

  alias AutofitplanBackend.LegacyProxy

  def index(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-sessions", method: "GET"})
  end

  def create(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-sessions", method: "POST"})
  end

  @spec show(atom | %{path_params: map}, any) :: any
  def show(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-sessions/#{id}", method: "GET"})
  end

  def update(conn, _) do
    id = Map.fetch!(conn.path_params, "id")
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-sessions/#{id}", method: "PATCH"})
  end

  def options(conn, _) do
    conn |> LegacyProxy.proxy(%{endpoint: "/api/logged-sessions", method: "OPTIONS"})
  end
end
