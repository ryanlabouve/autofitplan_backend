defmodule AutofitplanBackendWeb.Router do
  use AutofitplanBackendWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "http://localhost:4877"
    plug :accepts, ["json", "json-api"]
  end

  # Proxy
  resources "/ping", AutofitplanBackendWeb.PingController, only: [:index]

  scope "/api", AutofitplanBackendWeb do
    pipe_through(:api)
    ##############################################
    # Proxy
    ##############################################

    resources "/login", LoginController, only: [:create]
    options "/login", LoginController, :options

    resources "/token", TokenController, only: [:create]
    options "/token", TokenController, :options

    get "/users/me", UsersController, :me
    resources "/users", UsersController, only: [:index, :update, :show]
    options "/users", UsersController, :options
    options "/users/:id", UsersController, :options
    options "/users/me", UsersController, :options

    resources "/macrocycles", MacrocyclesController, only: [:index, :show]
    options "/macrocycles", MacrocyclesController, :options

    resources "/mesocycles", MesocyclesController, only: [:index, :show]
    options "/mesocycles", MesocyclesController, :options

    # resources "/microcycles", MesocyclesController, only: [:index, :show]
    # options "/microcycles", MesocyclesController, :options

    resources "/sessions", SessionsController, only: [:index, :show]
    options "/sessions", SessionsController, :options

    # resources "/exercises", ExercisesController, only: [:index]
    # options "/exercises", ExercisesController, :options

    resources "/logged-macrocycles", LoggedMacrocyclesController,
      only: [:index, :show, :create, :update, :delete]

    options "/logged-macrocycles", LoggedMacrocyclesController, :options
    options "/logged-macrocycles/:id", UsersController, :options

    resources "/logged-sessions", LoggedSessionsController,
      only: [:index, :show, :create, :update]

    options "/logged-sessions", LoggedSessionsController, :options
    options "/logged-sessions/:id", LoggedSessionsController, :options

    # resources "/logged-exercises", LoggedExercisesController, only: [:index, :show, :create, :update]
    # resources "/performance-tests", PerformanceTestsController

    resources "/home-screen-items", HomeScreenItemsController, only: [:index]
    options "/home-screen-items", HomeScreenItemsController, :options

    # resources "/daily-measurements", DailyMeasurementsController, only: [:index, :show]
    # resources "/logged-sets", LoggedSetsController, only: [:index, :show, :update]

    ##############################################
    # Converted
    ##############################################
  end

  scope "/", AutofitplanBackendWeb do
    pipe_through :browser
    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", AutofitplanBackendWeb do
  #   pipe_through :api
  # end
end
