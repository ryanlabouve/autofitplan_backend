defmodule AutofitplanBackend.LegacyProxy do
  @type opts :: binary | tuple | atom | integer | float | [opts] | %{opts => opts}
  @callback proxy(Plug.Conn.t(), opts) :: Plug.Conn.t()

  # push this complexity back into the module
  @legacy_proxy Application.get_env(:autofitplan_backend, :legacy_proxy)

  def proxy(conn, opts), do: @legacy_proxy.proxy(conn, opts)
end
