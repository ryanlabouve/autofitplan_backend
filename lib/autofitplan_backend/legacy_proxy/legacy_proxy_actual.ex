defmodule AutofitplanBackend.LegacyProxyActual do
  @behaviour LegacyProxy

  import Plug.Conn

  @impl LegacyProxy
  def proxy(conn, opts) do
    HTTPoison.start()
    url = "http://localhost:3009#{opts[:endpoint]}"
    # headers = [{"Content-Type", "application/vnd.api+json"}]
    headers = conn.req_headers
    body = Jason.encode!(conn.body_params)
    method = Map.fetch!(opts, :method)

    case proxy_request(url, method, headers, body) do
      {:ok, response} -> proxy_response(conn, response)
    end
  end

  defp proxy_request(url, "POST", headers, body) do
    options = [recv_timeout: 10_000]
    HTTPoison.post(url, body, headers, options)
  end

  defp proxy_request(url, "GET", headers, _) do
    HTTPoison.get(url, headers)
  end

  defp proxy_request(url, "PATCH", headers, body) do
    HTTPoison.patch(url, body, headers)
  end

  defp proxy_request(url, "DELETE", headers, body) do
    HTTPoison.delete(url, body, headers)
  end

  defp proxy_request(url, "OPTIONS", headers, _) do
    HTTPoison.options(url, headers)
  end

  defp proxy_response(conn, response) do
    {"Content-Type", ct} = response.headers |> List.keyfind("Content-Type", 0)

    conn
    |> put_resp_content_type(ct)
    |> send_resp(response.status_code, response.body)
    |> halt
  end
end
