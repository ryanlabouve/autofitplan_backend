defmodule AutofitplanBackend.Repo do
  use Ecto.Repo,
    otp_app: :autofitplan_backend,
    adapter: Ecto.Adapters.Postgres
end
