use Mix.Config

# Configure your database
config :autofitplan_backend, AutofitplanBackend.Repo,
  username: "postgres",
  password: "postgres",
  database: "autofitplan_backend_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :autofitplan_backend, AutofitplanBackendWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :autofitplan_backend, :legacy_proxy, AutofitplanBackend.LegacyProxyMock
